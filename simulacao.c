#include <stdio.h>
#define VOLTS 227
#define PRECO 0.55

int main() {

  float amperes = 30;

  float potencia = (VOLTS * amperes) / 1000;

  float segundo = PRECO / 60;

  printf("Custo por segundo: %.10f\n", segundo );

  printf("Potencia é %f kWatts\n", potencia);

  printf("Custo de uma hora R$ %.2f\n", PRECO * potencia);
  printf("Custo de 10 horas R$ %.2f\n", (PRECO * potencia) * 10);
  printf("Custo de 10 horas por 20 dias R$ %.2f\n", ((PRECO * potencia) * 10) * 20);

}
