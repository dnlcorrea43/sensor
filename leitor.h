#include "EmonLib.h"

int pino_sct = A0; //Pino do sensor SCT

EnergyMonitor emon1;

double leitura() {
    double leituras[10];

    emon1.current(pino_sct, 60);

    for (int i = 0; i < 10; i++) {

      // A m�gica
	    leituras[i] = emon1.calcIrms(1480);
	    Serial.printf("leitura: %f\n",leituras[i]);
	    delay(1000);
    }

    double media = 0;
    
    for (int i = 0; i < 10; i++) {
	    media += leituras[i];
    }

    return media / 10;
}
