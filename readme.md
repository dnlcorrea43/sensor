* Instalando as libs do Arduino *
** Versão do Arduino: 1.8.9 - tem como instalar bibliotecas.
** Board > Board Manager > ESP8266
** Library Manager (Ctrl+Shift+i) > Wifi101 e EmonLib

* Números
** Quando gasta no DB de espaço?
  - Uma leitura a cada segundo
  - uma chamada HTTP a cada 10 segundos
  - 6 por minuto
  - 360 por hora
  - 10 horas 3600
  - 7 dias de 10 horas = 25.200
  - 4 semanas  = 100.800 



* Usar Redis ou DB?
** Redis não persiste dados.
   Até persiste (de hora em hora) ou quando chama BGSAVE. Não dá pra
   perder dados caso caia a luz ou o server ou whatever. Fica eleito o
   database mesmo. 
