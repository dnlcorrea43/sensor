/**
	 Projet�o
	 Daniel
	 Cleyber
	 Stephany

	 Created on: 2019-05-06

*/
#include <Arduino.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include "leitor.h"
#include "EmonLib.h"

ESP8266WiFiMulti WiFiMulti;




char ssid[] = "Campus Boulevard";

char ip[] = "10.35.222.2";

WiFiClient client;

HTTPClient http;


void setup() {

	WiFi.mode(WIFI_STA);
	WiFiMulti.addAP(ssid);
	
	pinMode(LED_BUILTIN, OUTPUT);

	digitalWrite(LED_BUILTIN, LOW);

	Serial.begin(9600);

 //getIP();
}

void loop() {
  Serial.print("\n---------------\n");
		
	if (WiFiMulti.run() != WL_CONNECTED) return; // wait for WiFi connection
	
	double media = leitura();
	
	//double Irms = emon1.calcIrms(1480);
	
	Serial.printf("Corrente: %f a \n", media); 
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	//Serial.print("[HTTP] begin... --> http://" + ip + "/leituras\n");

	if (http.begin(client, "http://10.35.222.21/leituras")) {

		Serial.print("[HTTP] POST...\n");

		http.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
		int httpCode = http.POST("valor=" + String(media ,2) + "&aparelho_id=1");

		Serial.printf("HTTP Code: %d\n", httpCode);

		// httpCode will be negative on error
		if (httpCode > 0) {
			digitalWrite(LED_BUILTIN, HIGH);
			// HTTP header has been send and Server response header has been handled
			Serial.printf("[HTTP] POST code: %d\n", httpCode);

			// file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {

				String payload = http.getString();
				Serial.println(payload);
			} else {
				http.errorToString(httpCode).c_str();
			}
    }
    else {
			Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
		}

		http.end();
		digitalWrite(LED_BUILTIN, LOW);

	} else {
		Serial.printf("[HTTP] Unable to connect\n");
		digitalWrite(LED_BUILTIN, LOW);
	}

	//delay(2000);
}
